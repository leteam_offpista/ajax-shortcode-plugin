<?php

/*
Plugin Name: Ajax Shortcode
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 1.0
Author: VLZ
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

defined('ABSPATH') or die('No script kiddies please!');

class AjaxShortcode
{
    public function __construct()
    {
        add_action('wp_ajax_nopriv_ajax_shortcode', array($this, 'ajax_shortcode_callback'));
        add_action('wp_ajax_ajax_shortcode', array($this, 'ajax_shortcode_callback'));
        add_action('wp_enqueue_scripts', array($this, 'ajax_shortcode_JS_CSS'));
        add_shortcode('create_ajax_shortcode', array($this, 'create_ajax_shortcode'));

    }

    public static function create_ajax_shortcode($attrs)
    {
        $use_local = empty($attrs['use_local']) ? "data-use_local='false'" : "data-use_local='" . $attrs['use_local'] . "'";
        $expire = empty($attrs['expire_days']) ? "" : "data-expire_days='" . $attrs['expire_days'] . "'";
        if (empty($attrs['shortcode'])) {
            return '';
        }
        $ajx_shortcode = $attrs['shortcode'];
        if (empty($attrs['target'])) {
            $target_id = uniqid();
            $ajx_target = '#' . $target_id;
            $target_div = "<div id='$target_id'></div>";
        } else {
            $ajx_target = $attrs['target'];
            $target_div = "";
        }
        emsgd($attrs);
//        if (!$ajx_shortcode || !$ajx_target){
//            return '';
//        }
        return "
            <div
            class='sys-ajax-shortcode'
            data-ajx_shortcode = '$ajx_shortcode'
            data-ajx_target = '$ajx_target'
           $use_local
           $expire
            
            ></div>
            $target_div
            ";
    }

    public static function ajax_shortcode_JS_CSS()
    {
        $url = admin_url('admin-ajax.php');
        //we are in the bottom, so we can not waid documet.ready - our HTML shold be in place
        $JS = /** @lang JavaScript */
            <<<EOL
function ajax_shortcode_loader(els,force_ajax){
    
        var request=[];
        try{
            if (!$.isArray(els)){
                els = [els];
            }
            $.each(els,function(){
                var el=this;
                var data = {
                     target:el.dataset.ajx_target
                     ,shortcode:el.dataset.ajx_shortcode
                     ,dataset:el.dataset
                };
                var force = typeof force_ajax === 'undefined' ? false : force_ajax;
                if (force){
                     request.push(data);
                     return;
                }
                var use_LS = false;
                if (el.dataset.use_local === 'true' && window.localStorage){
                    use_LS = true;
                    try{
                        var ls = localStorage.getItem('bots-'+data.target);
                        if (!ls) {
                            console.debug('LocalStorage empty');
                            request.push(data);
                            return;
                        }
                        var resp = JSON.parse(ls);
                        if ((new Date() - new Date(resp.cached_on || 0))  /(24*3600*1000) > (el.dataset.expire_days || 10)){
                            console.debug('LocalStorage Expired');
                            request.push(data);
                            return;
                        }
                        $(data.target).html(resp.data).addClass('ls-loaded');
                        console.debug(data.shortcode+'_loaded fired');
                        $(document).trigger(data.shortcode+'_loaded');
                        console.debug('ls loaded '+data.shortcode);
                        
                    }catch (e){
                        console.debug(e.message);
                        request.push(data);
                    }
                }else{
                       request.push(data); 
                }
            })
        }catch(e){
            
        };
        if (!request.length){
            return null;
        }
		return $.ajax({
		    type:'GET',
			
			url:'$url',
			data:{action:'ajax_shortcode',shortcodes:request},
			success:function(resp_arr, textStatus, request){
				try {
				    $.each(resp_arr,function(){
                        try{
                            resp=this;
                            resp.cached_on=new Date();
                            localStorage.setItem('bots-'+resp.target,JSON.stringify(resp));
                            if (!$(resp.target).hasClass('ls-loaded')){
                                $(resp.target).html(resp.data);
                            }
                            console.debug(resp.shortcode+'_loaded fired');
                            $(document).trigger(resp.shortcode+'_loaded');
                            console.debug('ajx loaded '+resp.shortcode)

                        }	catch(e){
                            
                        }			        
				    })

				}catch(e) {

				}
			},
		dataType:'json'
		});
}	
function ajax_shortcode_process(){
    	banners=$('.sys-ajax-shortcode');
	// filter uniq targets, so it will not ajax for each 
	// element with .class targets
	var uniq_trgs=[];
	var el_list=[];
	banners.each(function(){
        if (uniq_trgs.indexOf(this.dataset.ajx_target) !== -1){
            return;
        }	  
        uniq_trgs.push(this.dataset.ajx_target);
        el_list.push(this);
	});
        ajax_shortcode_loader(el_list)
}
//
jQuery(document).ready(function($){
    ajax_shortcode_process();
});
EOL;

        self::enqueue_inline_script('ajax_shortcode_JS_CSS', $JS, array('jquery'), true);
    }
    /* ------------------------------------------- */
    /**
     * Enqueue inline Javascript. @see wp_enqueue_script().
     *
     * KNOWN BUG: Inline scripts cannot be enqueued before
     *  any inline scripts it depends on, (unless they are
     *  placed in header, and the dependant in footer).
     *
     * @param string $handle Identifying name for script
     * @param string $src The JavaScript code
     * @param array $deps (optional) Array of script names on which this script depends
     * @param bool $in_footer (optional) Whether to enqueue the script before </head> or before </body>
     *
     * @return null
     */
    function enqueue_inline_script($handle, $js, $deps = array(), $in_footer = false)
    {
        // Callback for printing inline script.
        $cb = function () use ($handle, $js) {
            // Ensure script is only included once.
            if (wp_script_is($handle, 'done')) {
                return;
            }
            // Print script & mark it as included.
            echo "<script type=\"text/javascript\" id=\"js-$handle\">\n$js\n</script>\n";
            global $wp_scripts;
            $wp_scripts->done[] = $handle;
        };
        // (`wp_print_scripts` is called in header and footer, but $cb has re-inclusion protection.)
        $hook = $in_footer ? 'wp_print_footer_scripts' : 'wp_print_scripts';

        // If no dependencies, simply hook into header or footer.
        if (empty($deps)) {
            add_action($hook, $cb);

            return;
        }

        // Delay printing script until all dependencies have been included.
        $cb_maybe = function () use ($deps, $in_footer, $cb, &$cb_maybe) {
            foreach ($deps as &$dep) {
                if (!wp_script_is($dep, 'done')) {
                    // Dependencies not included in head, try again in footer.
                    if (!$in_footer) {
                        add_action('wp_print_footer_scripts', $cb_maybe, 11);
                    } else {
                        // Dependencies were not included in `wp_head` or `wp_footer`.
                    }

                    return;
                }
            }
            call_user_func($cb);
        };
        add_action($hook, $cb_maybe, 0);
    }

    private function safe_die()
    {
        exit(0);
    }

    private function send_json($resp)
    {
        wp_send_json($resp); //return used fo PHPUnitTest
        //self::safe_die(); //Auto Died in wp_send_json
    }

    public function ajax_shortcode_callback()
    {
//        error_reporting(E_ALL);
//        ini_set('display_errors','on');

        $shortcodes = is_array($_GET['shortcodes']) ? $_GET['shortcodes'] : [$_GET['shortcodes']];
        //DDoS preventing  -limit to max 20 shortcodes
        $shortcodes = array_slice($shortcodes, 0, 20);
        $ret = [];

        foreach ($shortcodes as $s) {
            $ret[] = self::callback($s);
        }
        self::send_json($ret);
    }

    public function callback($params)
    {
        global $shortcode_tags;
        $resp = new stdClass();
        $target = $params['target'];
        $shortcode = $params['shortcode'];
        if (strpos($shortcode,'ajax_')!== 0){
            //only shortcodes started with 'ajax_' allowed for security reasons
            return '';
        }
//        emsgd($shortcode);
        $resp->target = $target;
        $resp->data = '';
//        emsgd($params);
        $atts = array();
        if (!empty($params['dataset']) && is_array($params['dataset'])) {
            foreach ($params['dataset'] as $k => $v) {
                if ($k == 'ajx_target' || $k == 'ajx_shortcode') {
                    continue;
                }
                $atts[] = $k . '="' . ($v) . '"';
            }
        }
        $atts = count($atts) ? ' ' . implode(' ', $atts) : '';
        //emsgd($shortcode_tags);
        foreach (array_keys($shortcode_tags) as $s) {
            if (strpos($s, 'ajax_') !== false && $s == $shortcode) {
                $sh = "[" . $s . $atts . "]";
                try {
                    if (defined("UNIT_TESTING")) {
                        do_shortcode($sh);
                    } else {
                        ob_start();
                        print(do_shortcode($sh));
                        $resp->data = ob_get_clean();
                        $resp->shortcode=$s;
                    }
                    break;

                } catch (Exception $e) {
//                    emsgd($e.message);
                }
            }
        }
        return $resp;
    }
}

new AjaxShortcode();
