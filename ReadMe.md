# Wordpress Plugin: Load content of shortcode via ajax-call
##Usage
### backend
for **security** reason only shortcodes which names starts with `ajax_` executed

### usage
####frontend
* add class `sys-ajax-shortcode` to element
* set `data-use_local='true'` to use [Local Storage](https://developer.mozilla.org/en/docs/Web/API/Window/localStorage) cache
    cache will be used if browser supports LocalStorage
* set     
* set `data-ajx_shortcode` to `shortcode name` to be retrieved    
* use `data-ajx_target` to specify css selector where retrived contnent will be placed
* use  `data-expire_days="X"` to set local storage expiration in days, set it to "-1" to prevent usage of cached content     
* use data-`param` to pass param to shortcode

each dom-element with class `sys-ajax-shortcode` considered as element which content should be loaded
via ajax call

plugin groups all short-codes that should be requested into one ajax-call, 
it groups by   `data-ajx_target` selector so u could update all elements that 
match `data-ajx_target` via one ajax-request

 example:
```html
 <div id="ajax_footer_popup_banner_v1"></div>
     <div
          class="sys-ajax-shortcode"
          data-ajx_shortcode="ajax_footer_popup_banner"
          data-ajx_target="#ajax_footer_popup_banner_v1"
          data-use_local="false"
          data-expire_days="-1"
     ></div>
```
  
  
####Wordpress shorcode
 it is possible to generate ajax-markup code via WP shortcode `create_ajax_shortcode`
```wordpress
 [create_ajax_shortcode 
  use_local='true'
  expire_days='2' 
  target="#ajax_footer_popup_banner_v1" 
  shortcode="ajax_recommended_broker_page_table"
  ]
```
 - if `target` param is missed, content will be placed in auto-generated `<div>` element exactly _after_ shortcode
 - use_local : default _false_
 - expire_days : default _none_
 

### on success loading
* class `ls-loaded` added to target dom-element
* `{shortcode name}_loaded` event triggered